<div class="thumbnail">
    <div class="thumb">
        <img src="{!! url('/task_results/'.$imgfile) !!}" alt="">
        <div class="caption-overflow">
            <span>
                <a href="/assets/images/placeholder.jpg" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
                <a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
            </span>
        </div>
    </div>

    <div class="caption">
        <h6 class="no-margin"><a href="#" class="text-default">{!! substr($imgfile, 37) !!}</a> <a href="#" class="text-muted"><i class="icon-three-bars pull-right"></i></a></h6>
    </div>
</div>