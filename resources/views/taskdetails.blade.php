@extends('layouts.app')

@section('themejs')
    <script type="text/javascript" src="/assets/js/core/libraries/jquery_ui/widgets.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/tables/datatables/extensions/natural_sort.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/forms/selects/select2.min.js"></script>

    <script src="/assets/amcharts/amcharts.js"></script>
    <script src="/assets/amcharts/serial.js"></script>
    <script src="/assets/amcharts//plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="/assets/amcharts/plugins/export/export.css" type="text/css" media="all" />
    <script src="/assets/amcharts/themes/light.js"></script>
@endsection
@section('pagejs')
    <script type="text/javascript" src="/assets/js/pages/taskdetails.js"></script>
@endsection

@section('maincontent')
    <script>
        id = '{!! $task->id !!}'
    </script>

    <div class="row">
        <div class="col-lg-12">
            <!-- Title with left icon -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title"><i class="icon-cog3 position-left"></i> Task details - <span class="text-muted">{!! $task->name !!}</span> </h6>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <p>Task created at <span class="samp">{!! $task->created_at !!}</span></p>

                            <div id="chartPerformance" style="width: 100%; height: 200px;"></div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title">Task related images</h6>
                </div>

                <div class="panel-body">
                    <div class="row">
                        @foreach($imgfiles as $imgfile)
                            @if (strpos($imgfile, 'avi') !== false)
                            <div class="col-lg-6 col-sm-12">
                                {{--@include('components.imgthumb', ['imgfile' => $imgfile])--}}
                                <?php echo basename($imgfile) ?><br>
                                {{--<video width="320" height="240" controls>--}}
                                    {{--<source src="/task_results/{!! $imgfile !!}" type="video/mp4">--}}
                                {{--</video>--}}
                                <object id="MediaPlayer" width="700" height="500" type="video/x-ms-asf">
                                    <param name="FileName"value="/task_results/{!! $imgfile !!}">
                                    <param name="autostart" value="false">
                                    <param name="ShowControls" value="true">
                                    <param name="ShowStatusBar" value="false">
                                    <param name="ShowDisplay" value="false">
                                    <param name="allowFullscreen" value="true">
                                    <embed type="application/x-mplayer2" src="/task_results/{!! $imgfile !!}"
                                           width="700" height="500" ShowControls="1" ShowStatusBar="0" ShowDisplay="0" autostart="0" />
                                </object>
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection