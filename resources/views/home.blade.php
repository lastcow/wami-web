@extends('layouts.app')

@section('pagejs')
    <script src="/assets/amcharts/amcharts.js"></script>
    <script src="/assets/amcharts/serial.js"></script>
    <script src="/assets/amcharts/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="/assets/amcharts/plugins/export/export.css" type="text/css" media="all" />
    <script src="/assets/amcharts/themes/light.jsh"></script>
    <script src="/assets/js/plugins/underscore-min.js"></script>
    <script src="/assets/js/plugins/moment.min.js"></script>
    <script type="text/javascript" src="/assets/js/pages/dashboard.js"></script>
@endsection

@section('maincontent')

    <div class="content-wrapper">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><b></b></h5>
                <img src="/img/IFT_image.jpg" class="img-responsive" height="120" width="100">
                <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

            <div class="panel-body">
                <h6 class="text-semibold"><b>Intelligent Fusion Technology (IFT)</b> is a Research and Development (R&D) company focused on information fusion technologies from basic research to industry transition and product development and support.</h6>
                <p class="content-group">All your dashboard contents goes here ... ...</p>

            </div>
        </div>

        <div class="panel panel-flat">
            <h4 class="text-semibold" align="center" style="padding-top:20px"><b>Recent Hadoop Execution Time</b></h4>
            <div id="chartdiv1" style="width:100%;height:400px;font-size:11px">
            </div>
            <h4 class="text-semibold" align="center" style="padding-top:50px"><b>Recent Uploaded Task Amount</b></h4>
            <div id="chartdiv2" style="width:100%;height:400px;font-size:11px">
            </div>

        </div>
        <!-- /simple panel -->
    </div>
@endsection
