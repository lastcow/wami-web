@extends('layouts.app')

@section('themejs')
    <script type="text/javascript" src="/assets/js/core/libraries/jquery_ui/widgets.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/tables/datatables/extensions/natural_sort.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/forms/selects/select2.min.js"></script>
@endsection
@section('pagejs')
    <script type="text/javascript" src="/assets/js/pages/tracking.js"></script>
    <script type="text/javascript" src="assets/js/plugins/uploaders/fileinput.min.js"></script>
    <script type="text/javascript" src="assets/js/pages/uploader_bootstrap.js"></script>

    <script type="text/javascript" src="assets/js/plugins/loaders/progressbar.min.js"></script>

    <script type="text/javascript" src="assets/js/plugins/velocity/velocity.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/velocity/velocity.ui.min.js"></script>

    <script type="text/javascript" src="assets/js/plugins/buttons/spin.js"></script>
    <script type="text/javascript" src="assets/js/plugins/buttons/ladda.min.js"></script>
    <script type="text/javascript" src="assets/js/pages/components_buttons.js"></script>

@endsection

@section('maincontent')
    <script>
        $('#menu_deep').addClass('active');
    </script>

    <div class="content-group">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h6 class="panel-title">Deep Learning Testing</h6>
            </div>

            <div class="panel-body">

                <div class="btn btn-success" id="btnNewTask" data-toggle="modal" data-target="#modal_upload_form_deeplearning">Deep Learning Sources</div>
            </div>
        </div>

        <form id="modal_upload_form_deeplearning" class="modal fade" action="deepsourceupload" method="post" enctype="multipart/form-data">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">Upload New Tasks</h5>
                    </div>

                    <div class="panel panel-body border-top-info">

                        <div class="form-group">
                            <label class="col-lg-2 control-label text-semibold">Multiple Files Allowed:</label>
                            <div class="col-lg-10">
                                <input type="file"  class="file-input-overwrite" name="deeplearning_uploadfile">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" id="btnSubmitDeepLearning" class="btn btn-primary btn-ladda btn-ladda-progress" data-style="expand-left" data-spinner-size="20"><span class="ladda-label">Submit</span></button>
                    </div>
                </div>
            </div>
        </form>

        <p class="text-muted text-size-small content-group">About 20 results (0.54 seconds)</p>

        <div class="search-results-list">
            <div class="row">

                        <div class="panel panel-white" id="panelDefault">
                            <div class="panel-body">
                                <div class="float-my-children" style="float:left;">
                                    <img src="assets/images/building.a.brainx910.jpg" alt="" style="width:250px;height:350px;">
                                </div>
                                <div style="margin-left:270px;">
                                    <p style="font-size:14px"><img src="assets/images/Restricted_Boltzmann_machine.png" width="200" height="260" style="float: right;border: 1px dotted black;margin-left:10px;">
                                        <b>Deep Learning</b> (also known as deep structured learning, hierarchical learning or deep machine learning)
                                    is a branch of machine learning based on a set of algorithms that attempt to model high level abstractions in data. In a simple case,
                                    you could have two sets of neurons: ones that receive an input signal and ones that send an output signal. When the input layer receives
                                    an input it passes on a modified version of the input to the next layer. In a deep network, there are many layers between the input and
                                    output (and the layers are not made of neurons but it can help to think of it that way), allowing the algorithm to use multiple processing
                                    layers, composed of multiple linear and non-linear transformations.</p>

                                    <p style="font-size:14px"><b>Deep Learning</b> is part of a broader family of machine learning methods based on learning representations of data. An observation (e.g., an
                                    image) can be represented in many ways such as a vector of intensity values per pixel, or in a more abstract way as a set of edges, regions
                                    of particular shape, etc. Some representations are better than others at simplifying the learning task (e.g., face recognition or facial
                                    expression recognition). One of the promises of deep learning is replacing handcrafted features with efficient algorithms for unsupervised
                                    or semi-supervised feature learning and hierarchical feature extraction.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-white" id="panelSearchResult">
                            <div class="panel-body">
                                <div class="progress content-group-sm" id="h-fill-basic">
                                    <div class="progress-bar progress-bar-primary" data-transitiongoal-backup="5" data-transitiongoal="100" style="width: 0%">
                                        <span class="sr-only">0%</span>
                                    </div>
                                </div>
                                {{--<button type="button" class="btn btn-primary btn-sm" id="h-fill-basic-start">Start Deep Learning Execution</button>--}}
                                {{--<button type="button" class="btn btn-danger btn-sm" id="h-fill-basic-reset">Reset</button>--}}
                            </div>
                        </div>

                    <div id="dl_imagedisplay"></div>

            </div>


        </div>
    </div>
    <!-- /search results -->


    <!-- Pagination -->
    <div class="text-center content-group">
        <ul class="pagination">
            <li class="disabled"><a href="#">&larr;</a></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">&rarr;</a></li>
        </ul>
    </div>


@endsection
