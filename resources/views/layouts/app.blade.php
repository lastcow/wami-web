<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>WAMI - Multi-Targets Tracking Web Portal</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="/assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    @yield('themejs')
    <script type="text/javascript" src="/assets/js/core/app.js"></script>

    <script type="text/javascript" src="/assets/js/plugins/ui/ripple.min.js"></script>
    <!-- /theme JS files -->

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

</head>

<body class="navbar-bottom">

<!-- GRAVATAR -->
<?php
$grav_url = ENV('GRAVATAR_PREFIX') . md5( strtolower( trim( Auth::user()->email ) ) ) . "?d=" . urlencode( "" ) . "&s=" . ENV('GRAVATAR_SIZE');
?>

<!-- Main navbar -->
<div class="navbar navbar-inverse bg-indigo">
    <div class="navbar-header">
        <a class="navbar-brand" href="index.html"><img src="/assets/images/logo_light.png" alt=""></a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">

            <li class="dropdown dropdown-user">
            @if(Auth::guest())
                <li><a href="{!! url('/login') !!}">SignIn</a></li>
            @else
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ $grav_url }}" alt="">
                    <span>{!! Auth::user()->name !!}</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
                    <li><a href="#"><span class="badge badge-warning pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                    <li><a href="{!! url('/logout') !!}"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
                @endif
                </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page header -->
<div class="page-header">

    <div class="page-header-content">
        <div class="page-title">
            <h4><span class="text-semibold">WAMI - Multi-Targets Tracking</span> - web portal</h4>
        </div>

    </div>
</div>
<!-- /page header -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main sidebar-default">
            <div class="sidebar-content">

                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">

                    <div class="sidebar-user-material">
                        <div class="category-content">
                            <div class="sidebar-user-material-content">
                                <a href="#"><img src="{{ $grav_url }}" class="img-circle img-responsive" alt=""></a>
                                @if(Auth::guest())
                                    <h6>Signup for more</h6>
                                @else
                                    <h6>{!! Auth::user()->name !!}</h6>
                                @endif
                                <span class="text-size-small">Software Engineer</span>
                            </div>

                            @if(!Auth::guest())
                                <div class="sidebar-user-material-menu">
                                    <a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
                                </div>
                            @endif
                        </div>
                        @if(!Auth::guest())
                            <div class="navigation-wrapper collapse" id="user-nav">
                                <ul class="navigation">
                                    <li><a href="#"><i class="icon-user-plus"></i> <span>My profile</span></a></li>
                                    <li><a href="#"><i class="icon-comment-discussion"></i> <span><span class="badge bg-teal-400 pull-right">58</span> Messages</span></a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="icon-cog5"></i> <span>Account settings</span></a></li>
                                    <li><a href="{!! url('/logout') !!}"><i class="icon-switch2"></i> <span>Logout</span></a></li>
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">

                            <!-- Main -->
                            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li id="menu_dashboard"><a href="{{ url('/dashboard') }}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                            <li id="menu_tracking"><a href="{{ url('/mttracking') }}"><i class="icon-archive"></i> <span>Multi-Targets Tracking</span></a></li>
                            <li id="menu_search"><a href="{{ url('/search') }}"><i class="icon-floppy-disk"></i> <span>Social Multimedia Querying</span></a></li>
                            <li id="menu_deep"><a href="{{ url('/deeplearning') }}"><i class="icon-link2"></i> <span>Deep Learning</span></a></li>
                            <!-- /main -->

                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        @yield('maincontent')
                <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->


<!-- Footer -->
<div class="navbar navbar-default navbar-fixed-bottom footer">
    <ul class="nav navbar-nav visible-xs-block">
        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="footer">
        <div class="navbar-text">
            &copy; 2016. <a href="http://www.i-fusion-i.com/aboutus/overview.htm" class="navbar-link" target="_blank">Intelligent Fusion Technology </a> by <a href="#" class="navbar-link" target="_blank">Sixiao Wei</a>
        </div>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="#">About</a></li>
                <li><a href="#">Terms</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- /footer -->

@yield('pagejs')
</body>
</html>

