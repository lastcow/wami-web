@extends('layouts.app')

@section('themejs')
    <script type="text/javascript" src="/assets/js/core/libraries/jquery_ui/widgets.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/tables/datatables/extensions/natural_sort.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/forms/selects/select2.min.js"></script>
@endsection
@section('pagejs')
    <script type="text/javascript" src="assets/js/pages/tracking.js"></script>
    <script type="text/javascript" src="assets/js/plugins/uploaders/fileinput.min.js"></script>
    <script type="text/javascript" src="assets/js/pages/uploader_bootstrap.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/styling/switch.min.js"></script>

    <script type="text/javascript" src="assets/js/core/app.js"></script>
    <script type="text/javascript" src="assets/js/pages/form_checkboxes_radios.js"></script>
    <script type="text/javascript" src="assets/js/plugins/ui/ripple.min.js"></script>


@endsection

@section('maincontent')
    <script>
        $('#menu_tracking').addClass('active');
    </script>

    <div class="content-wrapper">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h6 class="panel-title">Task Assignment</h6>
            </div>

            <div class="panel-body">
                <div class="btn btn-warning" id="btnNewTask" data-toggle="modal" data-target="#modal_form_vertical">Submit Task</div>&nbsp;&nbsp;
                <div class="btn btn-primary" id="btnNewTask" data-toggle="modal" data-target="#modal_upload_form">Upload Task</div>
            </div>
        </div>

        <form id="modal_upload_form" class="modal fade" action="taskupload" method="post" enctype="multipart/form-data">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">Upload New Tasks</h5>
                    </div>

                    <div class="panel panel-body border-top-info">

                        <div class="form-group">
                            <label class="col-lg-2 control-label text-semibold">Single File Allowed Only:</label>
                            <div class="col-lg-10">
                                <input type="file"  class="file-input-ajax" name="uploadfile">
                                <span class="help-block"><code>file-input</code>.</span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>

        <!-- Vertical form modal -->
        <form id="modal_form_vertical" class="modal fade" action="taskadd" method="get">
            <input type="hidden" name="whatevername" id="whatever">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">Start New Tasks</h5>
                    </div>

                    <div class="panel panel-body border-top-info">

                        <div class="form-group">
                            <label class="control-label col-lg-2"><b>Edit Task Name</b></label>
                            <div class="col-lg-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-inbox"></i></span>
                                    <input type="text" name="taskname" class="form-control" placeholder="Enter Task Name"><br>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2"><b>Method Selection</b></label>
                            <div class="col-lg-12">
                                <label class="radio-inline">
                                    <input type="radio" name="radio-inline-left" class="styled" value="0" checked="checked">
                                    GPU
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="radio-inline-left" class="styled" value="1">
                                    CPU
                                </label><br>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-2"><b>Task Source Selection</b></label>
                            <div class="col-lg-12">
                                <select name="filename" class="form-control">
                                    <option value="null">Please select uploaded task</option>
                                    @foreach($files1 as $file)
                                    <option value="{!! $file !!}">{!! $file !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- /vertical form modal -->

        <!-- Task manager table -->
        <div class="panel panel-white">
            <div class="panel-heading">
                <h6 class="panel-title">Task Manager</h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <table class="table tasks-list table-lg">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Period</th>
                    <th>Task Description</th>
                    <th>Execution Method</th>
                    <th>Priority</th>
                    <th>Latest update</th>
                    <th>Status</th>
                    <th>Assigned users</th>
                    <th class="text-center text-muted" style="width: 30px;"><i class="icon-checkmark3"></i></th>
                </tr>
                </thead>
                <tbody>

                <?php $index = 1 ?>
                @foreach($tasks as $task)
                    <tr>
                        <td>#{!! $index++ !!}</td>

                        <td>Today</td>
                        <td>
                            <div class="text-semibold"><a href="{!! url('/task/details/'.$task->id) !!}">{!! $task->name !!}</a></div>

                            <div class="text-muted">Real-time Hadoop Processing with GPU/CPU</div>
                        </td>
                        <td>
                            <div class="btn-group">
                                @if($task->exec_method == 0)
                                    <a href="#" class="label label-success dropdown-toggle">GPU</a>
                                @else
                                    <a href="#" class="label label-primary dropdown-toggle">CPU</a>
                                @endif
                            </div>
                        </td>
                        <td>
                            <div class="btn-group">
                                <a href="#" class="label label-danger dropdown-toggle" data-toggle="dropdown">Highest <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li class="active"><a href="#"><span class="status-mark position-left bg-danger"></span> Highest priority</a></li>
                                </ul>
                            </div>
                        </td>
                        <td>
                            <div class="input-group input-group-transparent">
                                <div class="input-group-addon"><i class="icon-calendar2 position-left"></i></div>
                                <div type="text">{!! $task->updated_at !!}</div>
                            </div>
                        </td>
                        <td>
                            <div name="status"><b>{!! $task->status !!}</b></div>
                        </td>
                        <td>
                            <a href="#"><img src="/img/IFT_image.jpg" class="img-circle img-xs" alt=""></a>
                            <a href="#" class="text-default">&nbsp;<i class="icon-plus22"></i></a>
                        </td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
                                    <ul class="dropdown-menu dropdown-menu-right" name="filename">
                                        <li><a href="taskdelete/{!! $task->id !!}"><i class="icon-cross2"></i>Remove</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
        <!-- /task manager table -->
    </div>
@endsection
