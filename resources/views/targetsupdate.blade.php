@extends('layouts.app')

@section('themejs')
    <script type="text/javascript" src="/assets/js/core/libraries/jquery_ui/widgets.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/tables/datatables/extensions/natural_sort.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/forms/selects/select2.min.js"></script>
@endsection
@section('pagejs')
    <script type="text/javascript" src="/assets/js/pages/tracking.js"></script>
@endsection

@section('maincontent')


@endsection
