@extends('layouts.app')

@section('themejs')
    <script type="text/javascript" src="/assets/js/core/libraries/jquery_ui/widgets.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/tables/datatables/extensions/natural_sort.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/forms/selects/select2.min.js"></script>
@endsection
@section('pagejs')
    <script type="text/javascript" src="/assets/js/pages/tracking.js"></script>
    <script type="text/javascript" src="assets/js/plugins/uploaders/fileinput.min.js"></script>
    <script type="text/javascript" src="assets/js/pages/uploader_bootstrap.js"></script>
@endsection

@section('maincontent')
    <script>
        $('#menu_search').addClass('active');
    </script>

    <div class="content-group">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h6 class="panel-title">Image Testing</h6>
            </div>

            <div class="panel-body">
                @if(empty($full_uploadFile))

                @else
                    <img src="{!! url('kitware_uploads/'.$full_uploadFile) !!}" alt="" style="width:250px;height:180px;"><br><br>
                @endif
                <div class="btn btn-primary" id="btnNewTask" data-toggle="modal" data-target="#modal_upload_form_kitware">Upload Image</div>
            </div>
        </div>

        <form id="modal_upload_form_kitware" class="modal fade" action="imageupload" method="post" enctype="multipart/form-data">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">Upload New Tasks</h5>
                    </div>

                    <div class="panel panel-body border-top-info">

                        <div class="form-group">
                            <label class="col-lg-2 control-label text-semibold">Single File Allowed Only:</label>
                            <div class="col-lg-10">
                                <input type="file"  class="file-input-overwrite" name="kitware_uploadfile">
                                <span class="help-block"><code>file-input</code>.</span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>

        <p class="text-muted text-size-small content-group">About 10 results (0.34 seconds)</p>

        <div class="search-results-list">
            <div class="row">

                <?php $index = 0 ?>
                @if(empty($nn_search))
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">Please Upload Image</h6>
                            </div>
                        </div>
                @else
                    @foreach($nn_search->distances as $element)
                        <div class="col-lg-3 col-sm-6">
                            <div class="thumbnail">
                                <div class="thumb">
                                    <img src="{!! url('kitware_images/'.$kitware_source[$index]->location) !!}" alt="" style="width:250px;height:200px;display: block;margin: 0 auto;">
                                    <?php $index++; $element_con = round($element*100,1) ?>
                                    <div class="caption-overflow">
                                    </div>
                                </div>

                                <div class="caption">
                                    <h6 class="text-semibold no-margin-top"><a href="#" class="text-default"><b>Similarity Degree:</b> {!! $element_con !!}%</a></h6>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>


        </div>
    </div>
    <!-- /search results -->


    <!-- Pagination -->
    <div class="text-center content-group">
        <ul class="pagination">
            <li class="disabled"><a href="#">&larr;</a></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">&rarr;</a></li>
        </ul>
    </div>


@endsection
