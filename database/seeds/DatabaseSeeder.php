<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        \App\Models\User::truncate();

        $user = new \App\Models\User();
        $user->name = 'sixiao wei';
        $user->email = 'whwsx910@gmail.com';
        $user->title = 'S/W engineer';
        $user->password = bcrypt('1234567890');

        $user->save();
    }
}
