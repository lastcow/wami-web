<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class ChartController extends Controller
{

    function getTaskTime(Request $request){
        // Get file upload from client

        $starttime = Carbon::now()->subMonths(3)->format('Y-m-d');

        // Get task
        
        $tasks = Task::where('created_at', '>=', $starttime)->whereNotIn('status', ['pending', 'new'])->get();

        return response()->json($tasks->toArray());

    }
}
