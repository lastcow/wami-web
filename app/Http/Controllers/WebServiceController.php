<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class WebServiceController extends Controller
{
    /**
     * Webservice: /task/get/{id}
     * @param $taskid
     */
    public function gettaskdetails($taskid){
        // Get task from DB
        try {
            $task = Task::findOrFail($taskid);
            return response()->json(['status'=>'Success', 'data'=>$task->toArray()]);
        }catch(\Exception $ex){
            return response()->json(['status'=>'Fail', 'msg'=>$ex->getMessage()]);
        }
    }


    /**
     * Webservice: /file/get/{id}
     * Get file from server
     * @param $taskid
     */
    public function getfile($taskid){

        // Get task record
        $task = Task::find($taskid);

        $file_path = public_path().'/uploads_pending/'.$task->filename;

        $response = response()->download($file_path, $task->filename, ['Content-Type'=>'video/avi']);

        ob_end_clean();

        return $response;
    }


    /**
     * Webservice: /upload/{tid}
     * Upload image files to front-end
     * @param $taskid
     */
    public function uploaddetails(Request $request, $tid){

        $imageFile = $request->file('imgfile');

        if(!$imageFile->isValid()){
            return 'not valid';
        }

        $diskResult = Storage::disk('task_results');

        if(! $diskResult->exists($tid)){
            // Make subfolder
            $diskResult->makeDirectory($tid.'/original');
            $diskResult->makeDirectory($tid.'/registration');
            $diskResult->makeDirectory($tid.'/detection');
            $diskResult->makeDirectory($tid.'/association');
            $diskResult->makeDirectory($tid.'/others');
        }

//        // Check for folder
//        if (! file_exists(public_path().'/task_results/'.$tid)){
//            // Create folder:
//            mkdir(public_path().'/task_results/'.$tid);
//        }

        $diskResult->put($tid.'/'.$imageFile->getClientOriginalName(), file_get_contents($imageFile));


//        $imageFile->move(public_path().'/task_results/'.$tid, $imageFile->getClientOriginalName());

    }



}
