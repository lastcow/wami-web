<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;

use App\Http\Requests;

class KitwareServiceController extends Controller
{
    public function index(){

        $user = Auth::getUser();

//        $client = new Client();
//        $res = $client->request('GET', '192.168.1.3:5001/nn/n=10/http://cdn.pixabay.com/photo/2012/05/29/00/43/car-49278_960_720.jpg', [
//
//        ]);

        //echo $res->getStatusCode();
        // "200"
        //echo json_encode($res->getHeader('content-type'));
        // 'application/json; charset=utf8'

//        $nn_search = json_decode($res->getBody());
//        if (empty($nn_search)){
//            return view('kitwareservice', ['nn_search' => $nn_search]);
//        }
//        else {
//            foreach ($nn_search->neighbors as $source){
//                $kitware_source[] = Image::whereId($source)->first();
//            }
//            return view('kitwareservice', ['nn_search' => $nn_search, 'kitware_source' => $kitware_source]);
//        }


        return view('kitwareservice');
    }

    public function imageupload(Request $request){

        $kitwareServerIp = env('KIT_WARE_SERVER_IP');
        $localServerIp = env('LOCAL_SERVER_IP');
        $kitwareServerPort = env('KIT_WARE_SERVER_PORT');
        $uploadFile = $request->file('kitware_uploadfile');

        $diskResult = Storage::disk('image_uploads');
        $carbon = new Carbon();

        if(! $diskResult->exists($uploadFile)){
            // Upload new kitware image content
            $diskResult->put($carbon->timestamp.'_'.$uploadFile->getClientOriginalName(), file_get_contents($uploadFile));
            $full_uploadFile = $carbon->timestamp.'_'.$uploadFile->getClientOriginalName();
        }
        else {
            $diskResult->put($carbon->timestamp.'_'.$uploadFile->getClientOriginalName(), file_get_contents($uploadFile));
            $full_uploadFile = $carbon->timestamp.'_'.$uploadFile->getClientOriginalName();
        }

        $client = new Client();
        $res = $client->request('GET', $kitwareServerIp.':'.$kitwareServerPort.'/nn/n=10/http://'.$localServerIp.'/kitware_uploads/'.$full_uploadFile, [

        ]);

        //http://cdn.pixabay.com/photo/2012/05/29/00/43/car-49278_960_720.jpg
        //echo $res->getStatusCode();
        // "200"
        //echo json_encode($res->getHeader('content-type'));
        // 'application/json; charset=utf8'
        $nn_search = json_decode($res->getBody());
        if (empty($nn_search)){
            return redirect()->action('KitwareServiceController@index', ['nn_search' => $nn_search]);
        }
        else {
            foreach ($nn_search->neighbors as $source){
                $kitware_source[] = Image::whereId($source)->first();
            }
            return view('kitwareservice', ['nn_search' => $nn_search, 'kitware_source' => $kitware_source, 'full_uploadFile' => $full_uploadFile]);
        }

    }
}
