<?php

namespace App\Http\Controllers;

//require 'vendor/autoload.php';
use Mailgun\Mailgun;
use App\Jobs\SubmitTask;
use App\Models\Task;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests;
use Carbon\Carbon;

use Illuminate\Support\Facades\Queue;

class TargetsTrackingController extends Controller
{

    /**
     * Authorized user only...
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){

        //$user = Auth::getUser();
//        if (file_exists(public_path().'/uploads/weekly-report-07-01-16.pdf')){
//            rename(public_path().'/uploads/weekly-report-07-01-16.pdf',public_path().'/uploads_pending/weekly-report-07-01-16.pdf');
//        }i
        $dir1    = public_path().'/uploads';
        $files1 = array_diff(scandir($dir1), array('..', '.'));

        $tasks = Task::all();
//        $dir2    = public_path().'/uploads_pending';
//        $files2 = array_diff(scandir($dir2), array('..', '.'));
//        $task_id = count($files2);
        //rename('weekly-report-07-01-16.pdf', 'C:\Users\Sixiao\Documents\wami-web\public\uploads_pending\weekly-report-07-01-16.pdf');
//        $processedFiles = Task::all();
        return view('targetstracking', ['files1' => $files1, 'tasks' => $tasks]);
    }


    public function movefile1(Request $request){

        $user = $request->user();
        if (null !== ($request->input('filename'))){
            $filename = $request->input('filename');
        }
        if (null !== ($request->input('taskname'))){
            $taskname = $request->input('taskname');
        }

        //$user = Auth::getUser();
        if (file_exists(public_path().'/uploads/'.$filename)){
            rename(public_path().'/uploads/'.$filename, public_path().'/uploads_pending/'.$filename);
        }

        try {
            // Save to DB.
            $task = new Task();
            $task->name = $request->input('taskname');
            $task->filename = $request->input('filename');
            $task->status = 'New';
            $task->owner()->associate($user);
            $task->exec_method = $request->input('radio-inline-left');

            $task->save();
            $data = ['tid'=>$task->id, 'require'=>$task->exec_method == '1'? 'CPU' : 'GPU'];
//            $this->dispatch(new SubmitTask($data));

//            Queue::push('app/Queues/SendEmail', array('message'=>'task submitted'));
            Queue::push(null, $data, 'ifx-pull');

            $task->status = 'Pending';
            $task->save();

        }catch(\Exception $ex){ Log::error($ex->getMessage());}

        return redirect()->action('TargetsTrackingController@index');
    }

    public function taskupload(Request $request){

        // Only reporting error.
        error_reporting(E_ERROR);

        $uploadFile = $request->file('uploadfile');

//        if(!$uploadFile->isValid()){
//            return 'not valid';
//        }

        $diskResult = Storage::disk('task_uploads');
        $carbon = new Carbon();

        if(! $diskResult->exists($uploadFile)){
            // Upload new task content
            $diskResult->put($carbon->timestamp.'_'.$uploadFile->getClientOriginalName(), file_get_contents($uploadFile));
        }
        else {
            $diskResult->put($carbon->timestamp.'_'.$uploadFile->getClientOriginalName(), file_get_contents($uploadFile));
        }
        return redirect()->action('TargetsTrackingController@index');
    }

    public function deletetask($taskid){
        // Get task
        try {
            $task = Task::findOrFail($taskid);
            $filename = $task->filename;
            $task->delete();

            // Delete file if existing.
            if (file_exists(public_path().'/uploads_pending/'.$filename)){
                unlink(public_path().'/uploads_pending/'.$filename);
            }
        }catch(\Exception $ex){}

        return redirect()->action('TargetsTrackingController@index');
    }

}
