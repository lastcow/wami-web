<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

use App\Http\Requests;

class TargetsUpdateController extends Controller
{

    /**
     * Authorized user only...
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request){

        $filename = $request->input('filename');

        //$user = Auth::getUser();
        if (file_exists(public_path().'/uploads/'.$filename)){
            rename(public_path().'/uploads/'.$filename, public_path().'/uploads_pending/'.$filename);
        }
        return redirect()->action('TargetsTrackingController@index');
    }


}


