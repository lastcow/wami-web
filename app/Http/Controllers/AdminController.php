<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{

    /**
     * Authorized user only.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Dashboard
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard(){

        $user =Auth::user();
//        Mail::send('emails.testreminder', ['user' => Auth::user()], function ($m) use ($user) {
//            $m->from('noreply@microcenter.com', 'noreply@microcenter.com');
//            $m->to($user->email, $user->name)->subject('Microcenter.com Pricing Information');
//        });
        return view('home');
    }
}
