<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;

use App\Http\Requests;

class DeepLearningController extends Controller
{
    public function index(){

        $user = Auth::getUser();

//        $client = new Client();
//        $res = $client->request('GET', '192.168.1.3:5001/nn/n=10/http://cdn.pixabay.com/photo/2012/05/29/00/43/car-49278_960_720.jpg', [
//
//        ]);

        //echo $res->getStatusCode();
        // "200"
        //echo json_encode($res->getHeader('content-type'));
        // 'application/json; charset=utf8'

//        $nn_search = json_decode($res->getBody());
//        if (empty($nn_search)){
//            return view('kitwareservice', ['nn_search' => $nn_search]);
//        }
//        else {
//            foreach ($nn_search->neighbors as $source){
//                $kitware_source[] = Image::whereId($source)->first();
//            }
//            return view('kitwareservice', ['nn_search' => $nn_search, 'kitware_source' => $kitware_source]);
//        }
        
        return view('deeplearning');
    }

    public function deepsourceupload(Request $request){

        $deep_uploadFile = $request->file('deeplearning_uploadfile');

        $deep_diskResult = Storage::disk('deep_uploads');
        $carbon = new Carbon();

//        if(! $deep_diskResult->exists($deep_uploadFile)){
//            // Upload new kitware image content
//            $deep_diskResult->put($carbon->timestamp.'_'.$deep_uploadFile->getClientOriginalName(), file_get_contents($deep_uploadFile));
//            $full_deep_uploadFile = $carbon->timestamp.'_'.$deep_uploadFile->getClientOriginalName();
//        }
//        else {
//            $deep_diskResult->put($carbon->timestamp.'_'.$deep_uploadFile->getClientOriginalName(), file_get_contents($deep_uploadFile));
//            $full_deep_uploadFile = $carbon->timestamp.'_'.$deep_uploadFile->getClientOriginalName();
//        }

        $dir_deep = public_path().'/deeplearning_images';
        $files_deep = array_diff(scandir($dir_deep), array('..', '.'));
        $fileCount = count($files_deep);

        return ['files_deep' => $files_deep, 'fileCount' => $fileCount];
        
    }
}
