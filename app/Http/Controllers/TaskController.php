<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class TaskController extends Controller
{
    /**
     * Update task status
     * @param Request $request
     */
    public function updateStatus(Request $request)
    {

        $tid = $request->input('tid');
        $status = $request->input('status');
        $etime = $request->input('execution_time');


        Log::info('Task status change request ......');
        Log::info('Task ID: ' . $tid + '   status: ' . $status);


        // Get task
        $task = Task::findOrFail($tid);
        $user = $task->owner;
        $task->status = $status;
        $task->execution_time = $etime;

        $task->save();

        if ($status == 'success') {
            Mail::send('emails.successreminder', ['user' => $user], function ($m) use ($user) {
                $m->from('Admin@intfusiontech.com', 'Hadoop Management Center');
                $m->to($user->email, $user->name)->subject('You Task has been completed!');
        });
        }

        if ($status == 'fail') {
            Mail::send('emails.failreminder', ['user' => $user], function ($m) use ($user) {
                $m->from('Admin@intfusiontech.com', 'Hadoop Management Center');
                $m->to($user->email, $user->name)->subject('You Task has been completed!');
            });
        }
    }


    /**
     * Show task details.
     * @param $tid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showDetails($tid){

        // Get task details
        $task = Task::find($tid);

        // Get images list
        $imgFiles = Storage::disk('task_results')->files($tid);

        return view('taskdetails', ['task'=>$task, 'imgfiles'=>$imgFiles]);
    }


    /**
     * Return json data of task statistics - CPU/GPU
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatisticsData(Request $request){
        $tid = $request->input('tid');
        //$tid='47119533-b072-4f29-adcb-6dff0d9731a9';

        $averageCPU = Task::where('exec_method', '1')->whereStatus('success')->avg('execution_time');
        $averageGPU = Task::where('exec_method', '0')->whereStatus('success')->avg('execution_time');

        $data = array();

        $task = Task::findOrFail($tid);
        // Task running time
        // if CPU is 1 and GPU is 0
        if($task->exec_method == '1'){
            $data[]=['name'=>'CPU Time', 'points'=>number_format((float)$task->execution_time, 2, '.', ''), 'color'=>'#7F8DA9', 'bullet'=>'/assets/images/hadoop_cpu.png'];
            $data[]=['name'=>'GPU Time', 'points'=>0, 'color'=>'#FEC514', 'bullet'=>'/assets/images/hadoop_gpu.png'];
            $data[]=['name'=>'Average CPU Time', 'points'=>number_format((float)$averageCPU, 2, '.', ''), 'color'=>'#DB4C3C', 'bullet'=>'/assets/images/hadoop_cpu.png'];
            $data[]=['name'=>'Average GPU Time', 'points'=>number_format((float)$averageGPU, 2, '.', ''), 'color'=>'#DAF0FD', 'bullet'=>'/assets/images/hadoop_gpu.png'];
        }
        else {
            $data[]=['name'=>'CPU Time', 'points'=>0, 'color'=>'#7F8DA9', 'bullet'=>'/assets/images/hadoop_cpu.png'];
            $data[]=['name'=>'GPU Time', 'points'=>number_format((float)$task->execution_time, 2, '.', ''), 'color'=>'#FEC514', 'bullet'=>'/assets/images/hadoop_gpu.png'];
            $data[]=['name'=>'Average CPU Time', 'points'=>number_format((float)$averageCPU, 2, '.', ''), 'color'=>'#DB4C3C', 'bullet'=>'/assets/images/hadoop_cpu.png'];
            $data[]=['name'=>'Average GPU Time', 'points'=>number_format((float)$averageGPU, 2, '.', ''), 'color'=>'#DAF0FD', 'bullet'=>'/assets/images/hadoop_gpu.png'];
        }

        return response()->json($data);
    }
}
