<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Queue;

Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('/', 'AdminController@dashboard');
Route::get('/dashboard', 'AdminController@dashboard');
Route::get('/mttracking', 'TargetsTrackingController@index');
Route::get('/search', 'KitwareServiceController@index');
Route::get('/deeplearning', 'DeepLearningController@index');

Route::post('/deepsourceupload', 'DeepLearningController@deepsourceupload');
Route::post('/imageupload', 'KitwareServiceController@imageupload');
Route::get('/taskadd', 'TargetsTrackingController@movefile1');
Route::post('/taskupload', 'TargetsTrackingController@taskupload');
Route::get('/taskdelete/{taskid}', 'TargetsTrackingController@deletetask');

Route::get('/task/get/{tid}', 'WebServiceController@gettaskdetails');
Route::get('/file/get/{tid}', 'WebServiceController@getfile');
Route::post('task/status', 'TaskController@updateStatus');
Route::get('/task/details/{tid}', 'TaskController@showDetails');

Route::post('/upload/{tid}', 'WebServiceController@uploaddetails');
Route::get('/getstatisticsdata/', 'TaskController@getStatisticsData');

Route::group(['prefix'=>'/ajax'], function(){
    Route::post('/task/bytime', 'ChartController@getTaskTime');
});

Route::post('queue/receive', function()
{
    return Queue::marshal();
});

