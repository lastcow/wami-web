<?php
/**
 * Created by PhpStorm.
 * User: chen3
 * Date: 7/28/16
 * Time: 3:29 PM
 */

namespace app\Queues;


use Illuminate\Support\Facades\Log;

class SendEmail
{

    public function fire($job, $data){

        Log::info($data);
    }

}