<?php

namespace App\Listeners;

use App\Events\TaskStart;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TaskStartEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TaskStart  $event
     * @return void
     */
    public function handle(TaskStart $event)
    {
        // Update database.

        // Send MQ
    }
}
