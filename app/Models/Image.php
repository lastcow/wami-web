<?php
/**
 * Created by PhpStorm.
 * User: chen3
 * Date: 6/27/16
 * Time: 4:48 PM
 */

namespace App\Models;


class Image extends BaseModel
{

    protected $table='images';

    public function owner(){
        return $this->belongsTo('App\Models\Image', 'owner_id');
    }

}