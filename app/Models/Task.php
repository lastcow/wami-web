<?php
/**
 * Created by PhpStorm.
 * User: chen3
 * Date: 6/27/16
 * Time: 4:48 PM
 */

namespace App\Models;


class Task extends BaseModel
{

    protected $table='task';

    public function owner(){
        return $this->belongsTo('App\Models\User', 'owner_id');
    }

}