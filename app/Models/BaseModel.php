<?php
/**
 * Created by PhpStorm.
 * User: chen3
 * Date: 4/6/16
 * Time: 4:37 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Webpatser\Uuid\Uuid;

class BaseModel extends Model
{


    // Never actually delete any result, only the mark.
    use SoftDeletes;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        /**
         * Attach to the 'creating' Model Event to provide a UUID
         * for the `id` field (provided by $model->getKeyName())
         */
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string)$model->generateNewId();
        });
    }

    /**
     *
     * Get a new version 4 (random) UUID.
     * @return Uuid
     * @throws \Exception
     */
    public function generateNewId()
    {
        // Generate version 4 truly random UUID;
        return Uuid::generate(4);
    }

}