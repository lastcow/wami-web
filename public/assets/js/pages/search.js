/**
 * Created by chen3 on 6/22/16.
 */

// Create an array with the values of all the input boxes in a column
$.fn.dataTable.ext.order['dom-text'] = function (settings, col) {
    return this.api().column(col, {order:'index'}).nodes().map( function (td, i) {
        return $('input', td).val();
    });
}


// Create an array with the values of all the select options in a column
$.fn.dataTable.ext.order['dom-select'] = function (settings, col) {
    return this.api().column(col, {order:'index'}).nodes().map( function (td, i) {
        return $('select', td).val();
    });
}

$(function(){

    // Menu
    $('#menu_search').addClass('active');

    // Table setup
    // ------------------------------
    

});