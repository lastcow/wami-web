/**
 * Created by chen3 on 6/22/16.
 */

var chart1;
var chart2;

$(function(){

    // Menu
    $('#menu_dashboard').addClass('active');

    $.post('/ajax/task/bytime', function(data){

        var data_grouped = _.groupBy(data, function(data){return moment(data.created_at).format('YYYY-MM-DD');})
        var data_mapped = _.map(data_grouped, function(data, key) {return {date: key, etime1: sum(_.pluck(data, 'execution_time')), etime2: sum(_.pluck(data, 'execution_time'))}});
        var data_count = _.countBy(data, function(data){return moment(data.created_at).format('YYYY-MM-DD');})
        var data_counted = _.map(data_count, function(data, key) {return {date: key, tasknum: data}});

        chart1.dataProvider = _.sortBy(data_mapped, 'date');
        chart1.validateData();
        chart2.dataProvider = _.sortBy(data_counted, 'date');
        chart2.validateData();
    });

    var chart1 = AmCharts.makeChart( "chartdiv1", {
        // "type": "serial",
        // "theme": "light",
        // "dataDateFormat": "YYYY-MM-DD",
        // "dataProvider": [],
        // "valueAxes": [ {
        //     "gridColor": "#FFFFFF",
        //     "gridAlpha": 0.2,
        //     "dashLength": 0,
        //     "title": "Hadoop Execution Time",
        //     unit: ' "',
        //     unitPosition: "right",
        //     position: "left"
        // } ],
        // "gridAboveGraphs": true,
        // "startDuration": 1,
        // "graphs": [ {
        //     "balloonText": "Total Hadoop Execution Time for [[category]]: <b>[[value]] Seconds</b>",
        //     "fillColorsField": "color",
        //     "fillAlphas": 0.8,
        //     "lineAlpha": 0.2,
        //     "type": "column",
        //     "valueField": "etime"
        // } ],
        // "chartCursor": {
        //     "categoryBalloonEnabled": false,
        //     "cursorAlpha": 0,
        //     "zoomable": false
        // },
        // "categoryField": "date",
        // "categoryAxis": {
        //     "gridPosition": "start",
        //     "gridAlpha": 0,
        //     "tickPosition": "start",
        //     "tickLength": 20
        //     //"labelRotation": 45
        // }

        "type": "serial",
        "addClassNames": true,
        "theme": "light",
        "dataDateFormat": "YYYY-MM-DD",
        "autoMargins": false,
        "marginLeft": 90,
        "marginRight": 0,
        "marginTop": 10,
        "marginBottom": 25,
        "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 8,
            "color": "#ffffff"
        },
        "dataProvider": [],
        "valueAxes": [ {
            "gridColor": "#FFFFFF",
            "gridAlpha": 0.2,
            "dashLength": 0,
            "title": "Hadoop Execution Time",
            unit: ' "',
            unitPosition: "right",
            position: "left"
        } ],
        "startDuration": 1,
        "graphs": [ {
            "alphaField": "alpha",
            "balloonText": "<span style='font-size:12px;'>Total Hadoop Execution Time for [[category]]:<br><span style='font-size:20px;'>[[value]] Seconds</span> [[additional]]</span>",
            "fillAlphas": 1,
            "type": "column",
            "valueField": "etime1",
            "dashLengthField": "dashLengthColumn"
        }, {
            "id": "graph2",
            "balloonText": "<span style='font-size:12px;'>Total Hadoop Execution Time for [[category]]:<br><span style='font-size:20px;'>[[value]] Seconds</span> [[additional]]</span>",
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "valueField": "etime2",
            "dashLengthField": "dashLengthLine"
        } ],
        "categoryField": "date",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "tickLength": 0
        }

    } );

    var chart2 = AmCharts.makeChart("chartdiv2", {
        "type": "serial",
        "theme": "light",
        "dataDateFormat": "YYYY-MM-DD",
        "dataProvider": [],
        "valueAxes": [ {
            "gridColor": "#FFFFFF",
            "gridAlpha": 0.2,
            "dashLength": 0,
            "precision": 0,
            "title": "Uploaded Task Amount",
            unit: ' ',
            unitPosition: "right",
            position: "left"
        } ],
        "gridAboveGraphs": true,
        "startDuration": 1,
        "graphs": [ {
            "balloonText": "Total Uploaded Tasks for [[category]]: <b>[[value]] Tasks</b>",
            "fillColorsField": "color",
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "tasknum"
        } ],
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": true
        },
        "categoryField": "date",
        "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0,
            "tickPosition": "start",
            "tickLength": 20
            //"labelRotation": 45

        }

    });
});


function sum(numbers) {
    return _.reduce(numbers, function(result, current) {
        return result + parseFloat(current);
    }, 0);
}

