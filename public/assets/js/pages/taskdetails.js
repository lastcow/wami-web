/**
 * Created by chen3 on 10/13/16.
 */


$(function(){

    // Menu highlight
    $('#menu_tracking').addClass('active');


    var chart = AmCharts.makeChart("chartPerformance",
        {
            "type": "serial",
            "theme": "light",
            "valueAxes": [{
                "maximum": 600,
                "minimum": 0,
                "axisAlpha": 0,
                "dashLength": 4,
                "position": "left"
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]] Seconds</b></span>",
                "bulletOffset": 10,
                "bulletSize": 52,
                "colorField": "color",
                "cornerRadiusTop": 8,
                "customBulletField": "bullet",
                "fillAlphas": 0.8,
                "lineAlpha": 0,
                "type": "column",
                "valueField": "points"
            }],
            "marginTop": 0,
            "marginRight": 0,
            "marginLeft": 0,
            "marginBottom": 0,
            "autoMargins": false,
            "categoryField": "name",
            "categoryAxis": {
                "axisAlpha": 0,
                "gridAlpha": 0,
                "inside": true,
                "tickLength": 0
            },
            "export": {
                "enabled": false
            }
        });

    $.get('/getstatisticsdata', {tid: id}, function (data){
        chart.dataProvider = data;
        chart.validateData();
    });

});