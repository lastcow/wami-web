/**
 * Created by chen3 on 6/22/16.
 */

// Create an array with the values of all the input boxes in a column
$.fn.dataTable.ext.order['dom-text'] = function (settings, col) {
    return this.api().column(col, {order:'index'}).nodes().map( function (td, i) {
        return $('input', td).val();
    });
}


// Create an array with the values of all the select options in a column
$.fn.dataTable.ext.order['dom-select'] = function (settings, col) {
    return this.api().column(col, {order:'index'}).nodes().map( function (td, i) {
        return $('select', td).val();
    });
}

$(function(){

    // Menu
    //$('#menu_tracking').addClass('active');

    $('#panelSearchResult').hide();

    // Table setup
    // ------------------------------

    // Initialize data table
    $('.tasks-list').DataTable({
        autoWidth: false,
        columnDefs: [
            {
                type: "natural",
                width: '20px',
                targets: 0
            },
            {
                visible: false,
                targets: 1
            },
            {
                width: '40%',
                targets: 2
            },
            {
                width: '10%',
                targets: 3
            },
            {
                orderDataType: 'dom-text',
                type: 'string',
                targets: 4
            },
            {
                orderDataType: 'dom-select',
                type: 'string',
                targets: 5
            },
            {
                orderable: false,
                width: '100px',
                targets: 7
            },
            {
                width: '15%',
                targets: [4, 5, 6]
            }
        ],
        order: [[ 0, 'desc' ]],
        dom: '<"datatable-header"fl><"datatable-scroll-lg"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        lengthMenu: [ 15, 25, 50, 75, 100 ],
        displayLength: 25,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page:'current'}).nodes();
            var last=null;

            // Grouod rows
            api.column(1, {page:'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                        '<tr class="active border-double"><td colspan="8" class="text-semibold">'+group+'</td></tr>'
                    );

                    last = group;
                }
            });

            // Datepicker
            $(".datepicker").datepicker({
                showOtherMonths: true,
                dateFormat: "d MM, y"
            });

            // Select2
            $('.select').select2({
                width: '150px',
                minimumResultsForSearch: Infinity
            });

            // Reverse last 3 dropdowns orientation
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function(settings) {

            // Reverse last 3 dropdowns orientation
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');

            // Destroy Select2
            $('.select').select2().select2('destroy');
        }
    });



    // External table additions
    // ------------------------------

    var $pb = $('#h-fill-basic .progress-bar');

    // Add placeholder to the datatable filter option
    $('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');


    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });

    $("input[name='radio-inline-left']").on('change', function() {
        exec_method = $('input[name=radio-inline-left]:checked', '.radio-inline').val();
        $('#whatever').val(exec_method);
    });

    $('#btnSubmitDeepLearning').on('click', function(e){

        $("#modal_upload_form_deeplearning").submit(function(e)
        {
            var postData = $(this).serializeArray();
            var formURL = $(this).attr("action");
            $.ajax(
                {
                    url : formURL,
                    type: "POST",
                    data : postData,
                    success:function(data, textStatus, jqXHR)
                    {
                        //$('#modal_upload_form_deeplearning').hide();
                        $("#modal_upload_form_deeplearning").modal('hide');
                        $('#panelDefault').hide();
                        $('#dl_imagedisplay').hide();
                        $('#panelSearchResult').show();

                        $pb.attr('data-transitiongoal', $pb.attr('data-transitiongoal-backup'));
                        $pb.progressbar({display_text: 'fill'});

                        var i=1;
                        var refreshId = setInterval(function() {
                                $('.progress-bar').attr("data-transitiongoal-backup", i*10);
                                $pb.attr('data-transitiongoal', $pb.attr('data-transitiongoal-backup'));
                                $pb.progressbar({display_text: 'fill'});
                                i++;
                            if (i >= 11) {
                                clearInterval(refreshId);
                                $('#dl_imagedisplay').show();
                            }
                            }, 5000);

                        //data: return data from server
                        $.each(data.files_deep, function(index, value){
                            //console.log(value);
                            $('#dl_imagedisplay').append(formatImageDiv(value, index-1));
                        });

                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        //if fails
                    }
                });
            e.preventDefault(); //STOP default action

        });
        
        // setTimeout(function() {
        //     $('.progress-bar').attr("data-transitiongoal-backup", "80");
        //         $pb.attr('data-transitiongoal', $pb.attr('data-transitiongoal-backup'));
        //         $pb.progressbar({display_text: 'fill'});
        //
        //         }, 5000);
        //     });

    });


    function formatImageDiv(imageurl, index) {
        var imgDiv =
            '<div class="col-lg-3 col-sm-6" id="display_dl">' +
            '<div class="thumbnail">' +
            '<div class="thumb">' +
            '<img src="deeplearning_images/' + imageurl + '" alt="" style="width:250px;height:200px;display: block;margin: 0 auto;">' +

            '<div class="caption-overflow">' +
            '</div>' +
            '</div>' +

            '<div class="caption">' +
            '<h6 class="text-semibold no-margin-top"><a href="#" class="text-default"><b>DL Result '+ index +' of 20</b></a></h6>' +
            '</div>' +
            '</div>' +
            '</div>';

        return imgDiv;
    }








    $('#h-fill-basic-start').click(function() {

    });

    // $('#aaa').click(function() {
    //     var aaa =  $(this);
    //     aaa.prop('disabled', true);
    //     setTimeout(function() {
    //         $('#aaa').submit();
    //     }, 5000);
    //     });
    // });



});

